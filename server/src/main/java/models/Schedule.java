package models;

public class Schedule {
    public Integer id;
    public String studentId;
    public String code;
    public String classCode;
    public String status = "not-final";  // enums: ["not-final", "submitted", "waiting"]

    public Schedule(
            Integer id,
            String StudentId,
            String code,
            String classCode,
            String status
    ) {
        this.id = id;
        this.studentId = StudentId;
        this.code = code;
        this.classCode = classCode;
        if (status != null) {
            this.status = status;
        }
    }
}
