package models;

import java.util.List;

public class SchedulePage {
    public List<Course> submitedCourses;
    public List<Course> selectedCourses;
    public List<Course> waitingCourses;
}
