package models;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import repository.ClassTimeRepository;
import repository.ExamTimeRepository;

public class Course {
    public int id;
    public String code;
    public String classCode;
    public String name;
    public int units;
    public String type;
    public String instructor;
    public int capacity;
    public List<String> prerequisites;
    public ClassTime classTime;
    public ExamTime examTime;

    public int signedUp = 0;

    public Course(
            Integer id,
            String code,
            String classCode,
            String name,
            Integer units,
            String type,
            String instructor,
            Integer capacity,
            String prerequisites,
            Integer classTimeID,
            Integer examTimeID
    ) {
        this.id = id;
        this.code = code;
        this.classCode = classCode;
        this.name = name;
        this.units = units;
        this.type = type;
        this.instructor = instructor;
        this.capacity = capacity;
        this.prerequisites = Arrays.asList(prerequisites.split("\\s*,\\s*"));
        try {
            this.classTime = ClassTimeRepository.getInstance().findById(classTimeID);
            this.examTime = ExamTimeRepository.getInstance().findById(examTimeID);
        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }
}
