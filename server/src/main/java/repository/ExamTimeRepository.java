package repository;

import models.ExamTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ExamTimeRepository extends Repository<ExamTime, Integer> {
    private static final String TABLE_NAME = "exam_time";
    private static ExamTimeRepository instance;

    public static ExamTimeRepository getInstance() {
        if (instance == null) {
            try {
                instance = new ExamTimeRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ExamTimeRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private ExamTimeRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id int NOT NULL AUTO_INCREMENT,\n" +
                                "start CHAR(50),\n" +
                                "end CHAR(50),\n" +
                                "UNIQUE KEY start_end_unique (start,end),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Integer id) throws SQLException {
        st.setInt(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (id, start, end) VALUES(?,?,?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, ExamTime data) throws SQLException {
        st.setInt(1, data.id);
        st.setString(2, data.start);
        st.setString(3, data.end);
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected ExamTime convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new ExamTime(
            rs.getInt(1),
            rs.getString(2),
            rs.getString(3)
        );
    }

    @Override
    protected ArrayList<ExamTime> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<ExamTime> examTimes = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            examTimes.add(this.convertResultSetToDomainModel(rs));
        }
        return examTimes;
    }

    public ExamTime findByValues(String start, String end) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByValueStatement());
        fillFindByValues(st, start, end);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            ExamTime result = convertResultSetToDomainModel(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    public String getFindByValueStatement() {
        return String.format("SELECT * FROM %s WHERE start = ? AND end = ?;", TABLE_NAME);
    }

    public void fillFindByValues(PreparedStatement st, String start, String end) throws SQLException {
        st.setString(1, start);
        st.setString(2, end);
    }
}
