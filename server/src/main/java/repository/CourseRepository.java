package repository;

import models.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import repository.ClassTimeRepository;
import repository.ExamTimeRepository;


public class CourseRepository extends Repository<Course, String> {
    private static final String TABLE_NAME = "courses";
    private static CourseRepository instance;

    public static CourseRepository getInstance() {
        if (instance == null) {
            try {
                instance = new CourseRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private CourseRepository() throws SQLException {
        ExamTimeRepository.getInstance();
        ClassTimeRepository.getInstance();

        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id int NOT NULL AUTO_INCREMENT,\n" +
                                "code CHAR(50),\n" +
                                "classCode CHAR(50),\n" +
                                "name CHAR(225),\n" +
                                "units INT,\n" +
                                "type ENUM('Asli', 'Paaye', 'Takhasosi', 'Umumi'),\n" +
                                "instructor CHAR(225),\n" +
                                "capacity INT,\n" +
                                "prerequisites CHAR(225),\n" +  // "8101xxx,8101xxx,8101xxx,8101xxx"    
                                "classTimeID INT,\n" +
                                "examTimeID INT,\n" +
                                "FOREIGN KEY (classTimeID) REFERENCES class_time(id),\n" +
                                "FOREIGN KEY (examTimeID) REFERENCES exam_time(id),\n" +
                                "UNIQUE KEY code_class (code, classCode),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format(
            "INSERT INTO %s " +
            "(id, code, classCode, name, units, type, instructor, capacity, prerequisites, classTimeID, examTimeID) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?);", TABLE_NAME
        );
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {
        st.setInt(1, data.id);
        st.setString(2, data.code);
        st.setString(3, data.classCode);
        st.setString(4, data.name);
        st.setInt(5, data.units);
        st.setString(6, data.type);
        st.setString(7, data.instructor);
        st.setInt(8, data.capacity);
        st.setString(9, String.join(",", data.prerequisites));

        ClassTimeRepository classTimeRepository = ClassTimeRepository.getInstance();
        ExamTimeRepository examTimeRepository = ExamTimeRepository.getInstance();

        try {
            classTimeRepository.insert(data.classTime);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            examTimeRepository.insert(data.examTime);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        int classTimeId = -1, examTimeId = -1;
        try {
            classTimeId = ClassTimeRepository.getInstance().findByValues(String.join(",", data.classTime.days), data.classTime.time).id;
            examTimeId = ExamTimeRepository.getInstance().findByValues(data.examTime.start, data.examTime.end).id;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }  

        if (classTimeId == -1 || examTimeId == -1) {
            throw new SQLException("Bad classTimeId or examTimeId");
        } else {
            st.setInt(10, classTimeId);
            st.setInt(11, examTimeId);
        }
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Course(
            rs.getInt(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getInt(5),
            rs.getString(6),
            rs.getString(7),
            rs.getInt(8),
            rs.getString(9),
            rs.getInt(10),
            rs.getInt(11)
        );
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Course> courses = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            courses.add(this.convertResultSetToDomainModel(rs));
        }
        return courses;
    }

    public Course findByCode(String code) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByCodeStatement());
        fillFindByCode(st, code);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            Course result = convertResultSetToDomainModel(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getFindByCodeStatement() {
        return String.format("SELECT * FROM %s WHERE code = ?;", TABLE_NAME);
    }

    protected void fillFindByCode(PreparedStatement st, String code) throws SQLException {
        st.setString(1, code);
    }
}
