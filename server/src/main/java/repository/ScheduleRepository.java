package repository;

import models.ClassTime;
import models.Schedule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ScheduleRepository extends Repository<Schedule, String> {
    private static final String TABLE_NAME = "schedules";
    private static ScheduleRepository instance;

    public static ScheduleRepository getInstance() {
        if (instance == null) {
            try {
                instance = new ScheduleRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ScheduleRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private ScheduleRepository() throws SQLException {
        ExamTimeRepository.getInstance();
        ClassTimeRepository.getInstance();

        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id int NOT NULL AUTO_INCREMENT,\n" +
                                "studentId CHAR(50),\n" +
                                "code CHAR(50),\n" +
                                "classCode CHAR(50),\n" +
                                "status ENUM('not-final', 'submitted', 'waiting'),\n" +
                                "FOREIGN KEY (studentId) REFERENCES students(id),\n" +
                                "UNIQUE KEY code_class (StudentId, code, classCode),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format(
            "INSERT INTO %s " +
            "(studentId, code, classCode, status) " +
            "VALUES(?,?,?,?);", TABLE_NAME
        );
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Schedule data) throws SQLException {
        st.setString(1, data.studentId);
        st.setString(2, data.code);
        st.setString(3, data.classCode);
        st.setString(4, data.status);
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected Schedule convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Schedule(
            rs.getInt(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getString(5)
        );
    }

    @Override
    protected ArrayList<Schedule> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Schedule> schedules = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            schedules.add(this.convertResultSetToDomainModel(rs));
        }
        return schedules;
    }

    public List<Schedule> findByStudentId(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByStudentIdStatement());
        fillFindByValues(st, studentId);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            List<Schedule> result = convertResultSetToDomainModelList(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getFindByStudentIdStatement() {
        return String.format("SELECT * FROM %s WHERE studentId = ?;", TABLE_NAME);
    }

    protected void fillFindByValues(PreparedStatement st, String studentId) throws SQLException {
        st.setString(1, studentId);
    }

    public int getCourseSignedUpCount(String code, String classCode) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getCourseSignedUpCountStatement());
        fillFindByCodeAndClassCode(st, code, classCode);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return 0;
            }
            List<Schedule> result = convertResultSetToDomainModelList(resultSet);
            st.close();
            con.close();
            return result.size();
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getCourseSignedUpCountStatement() {
        return String.format("SELECT * FROM %s WHERE code = ? AND classCode = ?;", TABLE_NAME);
    }

    protected void fillFindByCodeAndClassCode(PreparedStatement st, String code, String classCode) throws SQLException {
        st.setString(1, code);
        st.setString(2, classCode);
    }

    public void deleteSchedule(String studentId, String code, String classCode) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getScheduleDeleteStatement());
        fillDeleteScheduleStatement(st, studentId, code, classCode);
        try {
            st.executeUpdate();
            st.close();
            con.close();
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.delete query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getScheduleDeleteStatement() {
        return String.format("DELETE FROM %s WHERE studentId = ? AND code = ? AND classCode = ?;", TABLE_NAME);
    }

    protected void fillDeleteScheduleStatement(PreparedStatement st, String studentId, String code, String classCode) throws SQLException {
        st.setString(1, studentId);
        st.setString(2, code);
        st.setString(3, classCode);
    }
}
