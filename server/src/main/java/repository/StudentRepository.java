package repository;

import models.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class StudentRepository extends Repository<Student, String> {
    private static final String TABLE_NAME = "students";
    private static StudentRepository instance;

    public static StudentRepository getInstance() {
        if (instance == null) {
            try {
                instance = new StudentRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in StudentRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private StudentRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id CHAR(50) UNIQUE,\n" +
                                "name CHAR(225),\n" +
                                "secondName CHAR(225),\n" +
                                "birthDate CHAR(50),\n" +
                                "field CHAR(50),\n" +
                                "faculty CHAR(50),\n" +
                                "level CHAR(50),\n" +
                                "status CHAR(50),\n" +
                                "img CHAR(225),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s " +
                "(id, name, secondName, birthDate, field, faculty, level, status, img) " +
                "VALUES(?,?,?,?,?,?,?,?,?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Student data) throws SQLException {
        st.setString(1, data.id);
        st.setString(2, data.name);
        st.setString(3, data.secondName);
        st.setString(4, data.birthDate);
        st.setString(5, data.field);
        st.setString(6, data.faculty);
        st.setString(7, data.level);
        st.setString(8, data.status);
        st.setString(9, data.img);
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected Student convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Student(
            rs.getString(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getString(5),
            rs.getString(6),
            rs.getString(7),
            rs.getString(8),
            rs.getString(9)
        );
    }

    @Override
    protected ArrayList<Student> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Student> students = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            students.add(this.convertResultSetToDomainModel(rs));
        }
        return students;
    }
}
