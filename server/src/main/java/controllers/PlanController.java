package controllers;

import data.DataHandler;
import models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class PlanController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/plan", method = RequestMethod.GET)
    public ResponseEntity getPlan() {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        List<Schedule> studentSchedules = DataHandler.getPlanScheduleForStudent(Server.server.getLoggedInStudent().id);
        String[][] tableEntries = new String[5][5];
        
        if (studentSchedules == null)
            return ResponseEntity.ok(tableEntries);

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                tableEntries[i][j] = "";
            }
        }

        for (Schedule schedule: studentSchedules) {
            Course course = DataHandler.getCourse(schedule.code);
            List<Integer> rows = new ArrayList<>();
            List<Integer> columns = new ArrayList<>();

            course.classTime.days.replaceAll(String::trim);
            assert course != null;

            for (String day: course.classTime.days) {
                switch (day) {
                    case "Saturday":
                        rows.add(0);
                        break;
                    
                    case "Sunday":
                        rows.add(1);
                        break;

                    case "Monday":
                        rows.add(2);
                        break;

                    case "Tuesday":
                        rows.add(3);
                        break;

                    case "Wednesday":
                        rows.add(4);
                        break;
                
                    default:
                        break;
                }
            }

            course.classTime.time = course.classTime.time.trim();
            for (int row: rows) {
                if (course.classTime.time.equals("7:30-9:00")) {
                    tableEntries[row][0] = course.name;
                }
                if (course.classTime.time.equals("9:00-10:30")) {
                    tableEntries[row][1] = course.name;
                }
                if (course.classTime.time.equals("10:30-12:00")) {
                    tableEntries[row][2] = course.name;
                }
                if (course.classTime.time.equals("14:00-15:30")) {
                    tableEntries[row][3] = course.name;
                }
                if (course.classTime.time.equals("16:00-17:30")) {
                    tableEntries[row][4] = course.name;
                }
            }
        }
        return ResponseEntity.ok(tableEntries);
    }
}
