package controllers;

import models.Student;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import server.Server;
import org.json.JSONException;
import org.json.JSONObject;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class LoginController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity login (HttpServletRequest req) {
        try {
            String requestData = req.getReader().lines().collect(Collectors.joining());
            JSONObject body = new JSONObject(requestData);
            String studentId = body.getString("std_id");

            if (StudentsService.loginIfExists(studentId)) {
                return ResponseEntity.ok("Logged in successfully!");
            } else {
                return new ResponseEntity<>("No student with this id!", HttpStatus.BAD_REQUEST);
            }
        } catch (JSONException | IOException e) {
            System.out.println(e.toString());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
