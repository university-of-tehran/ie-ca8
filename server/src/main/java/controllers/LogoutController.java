package controllers;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class LogoutController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public ResponseEntity login (HttpServletRequest req) {
        StudentsService.logout();
        return ResponseEntity.ok("Logged out successfully!");
    }
}
