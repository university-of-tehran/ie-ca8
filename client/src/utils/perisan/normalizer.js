const persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

const convertNumbersToPersian = (str) => {
    if (typeof str !== "string") {
        str = String(str);
    }

    const numbers = [...Array(10).keys()];

    for (const n of numbers) {
        str = str.replaceAll(n, persianNumbers[n]);
    }

    return str;
};

export { convertNumbersToPersian };
