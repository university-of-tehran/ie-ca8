import React, { Component } from "react";

import { convertNumbersToPersian } from "../../../utils/perisan/normalizer";

import logo from "../../../assets/logo.png";

export default class ProfileSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            student: props.student,
            gpa: props.gpa,
            totalPassedUnits: props.totalPassedUnits,
            render: props.render,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            student: nextProps.student,
            gpa: nextProps.gpa,
            totalPassedUnits: nextProps.totalPassedUnits,
            render: nextProps.render,
        });  
      }

    render() {
        const { student, gpa, totalPassedUnits } = this.state;

        if (!this.state.render) return null;

        return (
            <div className="middle-sidebar">
                <img
                    className="profile-pic mb-4"
                    src={logo}
                    alt="profile pic"
                />
                <div className="profile-info">
                    <div className="info-key">نام:</div>
                    <div className="info-value">
                        {`${student.name} ${student.secondName}`}
                    </div>
                </div>
                <div className="profile-info">
                    <div className="info-key">شماره دانشجویی:</div>
                    <div className="info-value">
                        {convertNumbersToPersian(student.id)}
                    </div>
                </div>
                <div className="profile-info">
                    <div className="info-key">تاریخ تولد:</div>
                    <div className="info-value">
                        {convertNumbersToPersian(student.birthDate)}
                    </div>
                </div>
                <div className="profile-info">
                    <div className="info-key">معدل کل:</div>
                    <div className="info-value">
                        {convertNumbersToPersian(gpa)}
                    </div>
                </div>
                <div className="profile-info">
                    <div className="info-key">واحد گذرانده:</div>
                    <div className="info-value">
                        {convertNumbersToPersian(totalPassedUnits)}
                    </div>
                </div>

                <div className="student-status">مشغول به تحصیل</div>
            </div>
        );
    }
}
