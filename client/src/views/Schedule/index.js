import React, { Component } from "react";
import axios from "axios";

import Header from "../../shared-components/Header";
import Footer from "../../shared-components/Footer";
import ImageSlider from "../../shared-components/ImageSlider";
import { convertNumbersToPersian } from "../../utils/perisan/normalizer";

import "./style.css";

export default class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            plan: [],
        };
    }

    componentDidMount() {
        axios.get("http://localhost:8080/plan").then(
            (response) => {
                console.log(response.data);
                this.setState({
                    plan: response.data,
                });
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        const plan = this.props.plan;
        return (
            <>
                <Header />
                <ImageSlider />
                <div class="middle">
                    <div class="middle-content">
                        <div class="schedule">
                            <div class="schedule-header">
                                <div class="schedule-header-left">ترم فعلی</div>
                                <div class="schedule-header-right">
                                    <div>برنامه هفتگی</div>
                                    <i class="flaticon-calendar"></i>
                                </div>
                            </div>

                            <div class="schedule-row">
                                <div class="schedule-cell"></div>
                                <div class="schedule-cell">شنبه</div>
                                <div class="schedule-cell">یک‌شنبه</div>
                                <div class="schedule-cell">دوشنبه</div>
                                <div class="schedule-cell">سه‌شنبه</div>
                                <div class="schedule-cell">چهارشنبه</div>
                                <div class="schedule-cell">پنج‌شنبه</div>
                            </div>

                            {[...Array(11).keys()].map((n) => {
                                let startingHour = n + 7;
                                let endingOur = startingHour + 1;
                                return (
                                    <div class="schedule-row">
                                        <div class="schedule-cell">
                                            {`${convertNumbersToPersian(
                                                startingHour
                                            )}:۰۰ - ${convertNumbersToPersian(
                                                endingOur
                                            )}:۰۰`}
                                        </div>
                                        {[...Array(5).keys()].map((day) => {
                                            let plan = this.state.plan;
                                            let classInSchedule = null;

                                            if (!plan.length || !plan)
                                                return (
                                                    <div class="schedule-cell"></div>
                                                );

                                            switch (startingHour) {
                                                case 7:
                                                    classInSchedule =
                                                        plan[day][0];
                                                    break;
                                                case 9:
                                                    classInSchedule =
                                                        plan[day][1];
                                                    break;
                                                case 10:
                                                    classInSchedule =
                                                        plan[day][2];
                                                    break;
                                                case 14:
                                                    classInSchedule =
                                                        plan[day][3];
                                                    break;
                                                case 16:
                                                    classInSchedule =
                                                        plan[day][4];
                                                    break;

                                                default:
                                                    break;
                                            }

                                            if (!classInSchedule) {
                                                return (
                                                    <div class="schedule-cell"></div>
                                                );
                                            }
                                            let halfwayStart = [7, 10].includes(
                                                startingHour
                                            );

                                            let classTimeText = convertNumbersToPersian(
                                                `${
                                                    `${startingHour}:${halfwayStart
                                                        ? "30"
                                                        : "00"}
                                                    `
                                                }-${
                                                    `${endingOur}:${halfwayStart
                                                        ? "00"
                                                        : "30"}
                                                    `
                                                }`
                                            );
                                            return (
                                                <div class="schedule-cell">
                                                    <div
                                                        class={`lesson length-1-5 ${
                                                            halfwayStart
                                                                ? "halfway-start color-blue"
                                                                : "normal-start color-green"
                                                        }`}
                                                    >
                                                        <div class="lesson-info-row">
                                                            {classTimeText}
                                                        </div>
                                                        <div class="lesson-info-row">
                                                            {classInSchedule}
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                        <div class="schedule-cell"></div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        );
    }
}
