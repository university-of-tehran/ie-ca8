import React from "react";

import { convertNumbersToPersian } from "../../../utils/perisan/normalizer";

export default function TableRowCourses(props) {
    const course = props.course;
    const state = props.state;
    const handleRemoveCourse = () => props.handleRemoveCourse(course.code);

    return (
        <div class="table-row-courses">
            <div class="table-cell cell-delete" onClick={handleRemoveCourse}>
                <i class="flaticon-trash-bin"> </i>
            </div>
            <div class="table-cell cell-status">
                <div class={`status-value status-${state.className}`}>{state.text}</div>
            </div>
            <div class="table-cell cell-code">{course.code}</div>
            <div class="table-cell cell-name">{course.name}</div>
            <div class="table-cell cell-teacher">
                {course.Instructor ? course.Instructor : "نامشخص"}
            </div>
            <div class="table-cell cell-units">
                {convertNumbersToPersian(course.units)}
            </div>
        </div>
    );
}
