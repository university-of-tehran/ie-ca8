import React, { Component } from "react";
import axios from "axios";

import Header from "../../shared-components/Header";
import Footer from "../../shared-components/Footer";
import { convertNumbersToPersian } from "../../utils/perisan/normalizer";
import TableRowCourses from "./components/TableRowCourses";

import "./style.css";

const courseTypes = {
    Asli: { fa: "اصلی", className: "main" },
    Ekhtiary: { fa: "اختیاری", className: "base" },
    Takhasosi: { fa: "تخصصی", className: "specialty" },
    Umumi: { fa: "عمومی", className: "general" },
};

const submitActions = {
    RESET: "reset",
    SUBMIT: "submit",
};

export default class Courses extends Component {
    constructor(props) {
        super(props);

        this.state = {
            courses: [],
            search: "",
            typeFilter: "",
            selectedCourses: [],
            submittedCourses: [],
            waitingCourses: [],
        };

        this.searchCourses = this.searchCourses.bind(this);
        this.handleAddCourse = this.handleAddCourse.bind(this);
        this.handleRemoveCourse = this.handleRemoveCourse.bind(this);
        this.getSchedule = this.getSchedule.bind(this);
        this.getTotalSubmittedUnits = this.getTotalSubmittedUnits.bind(this);
        this.getCourseState = this.getCourseState.bind(this);
        this.setTypeFilter = this.setTypeFilter.bind(this);
    }

    getSchedule() {
        axios.get("http://localhost:8080/schedule").then(
            (response) => {
                const data = response.data;
                this.setState({
                    submittedCourses: data.submitedCourses,
                    selectedCourses: data.selectedCourses,
                    waitingCourses: data.waitingCourses,
                });
            },
            (error) => {
                console.log(error);
            }
        );
    }

    searchCourses() {
        axios
            .get(
                `http://localhost:8080/search?action=search&search=${this.state.search}`
            )
            .then(
                (response) => {
                    this.setState({ courses: response.data });
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    handleAddCourse(course) {
        axios
            .post(`http://localhost:8080/schedule/add`, {
                course_code: course.code,
                class_code: 1,
            })
            .then(
                (response) => {
                    window.location.reload();
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    handleRemoveCourse(courseCode) {
        axios
            .delete(
                `http://localhost:8080/schedule/delete?course_code=${courseCode}&class_code=1`
            )
            .then(
                (response) => {
                    window.location.reload();
                },
                (error) => {
                    console.log(error.data);
                }
            );
    }

    handleSubmit(action) {
        axios
            .post(`http://localhost:8080/schedule/submit?action=${action}`)
            .then(
                (response) => {
                    window.location.reload();
                },
                (error) => {
                    console.log(error.data);
                }
            );
    }

    getTotalSubmittedUnits() {
        let totalUnits = 0;
        for (const course of this.state.submittedCourses) {
            totalUnits += course.units;
        }

        return totalUnits;
    }

    getCourseState(course) {
        for (const c of this.state.waitingCourses) {
            if (c.code == course.code) {
                return { className: "waiting", text: "در انتظار" };
            }
        }

        for (const c of this.state.submittedCourses) {
            if (c.code == course.code) {
                return { className: "submitted", text: "ثبت شده" };
            }
        }

        return { className: "not-final", text: "ثبت‌نهایی نشده" };
    }

    setTypeFilter(type) {
        return (event) => {
            event.preventDefault();
            this.setState({typeFilter: type})
        }
    }

    componentDidMount() {
        this.getSchedule();
        this.searchCourses();
    }

    render() {
        const {waitingCourses, selectedCourses, typeFilter} = this.state;

        return (
            <>
                <Header />
                <div class="middle middle-courses">
                    <div class="middle-content">
                        <div class="table-wrapper-courses selected-courses">
                            <div class="table-title-courses">
                                دروس انتخاب شده
                            </div>
                            <div class="table-rows-container-courses">
                                <div class="table-row-courses">
                                    <div class="table-cell cell-delete cell-title"></div>
                                    <div class="table-cell cell-status cell-title">
                                        وضعیت
                                    </div>
                                    <div class="table-cell cell-code cell-title">
                                        کد
                                    </div>
                                    <div class="table-cell cell-name cell-title">
                                        نام درس
                                    </div>
                                    <div class="table-cell cell-teacher cell-title">
                                        استاد
                                    </div>
                                    <div class="table-cell cell-units cell-title">
                                        واحد
                                    </div>
                                </div>
                                {selectedCourses.map((course) => (
                                    <TableRowCourses
                                        course={course}
                                        handleRemoveCourse={
                                            this.handleRemoveCourse
                                        }
                                        state={this.getCourseState(course)}
                                    />
                                ))}
                                {waitingCourses.map((course) => (
                                    <TableRowCourses
                                        course={course}
                                        handleRemoveCourse={
                                            this.handleRemoveCourse
                                        }
                                        state={this.getCourseState(course)}
                                    />
                                ))}
                            </div>
                            <div class="table-footer-courses">
                                <div class="submitted-units-count">
                                    تعداد واحد ثبت شده:{" "}
                                    {convertNumbersToPersian(
                                        this.getTotalSubmittedUnits()
                                    )}
                                </div>
                                <div class="footer-buttons">
                                    <div
                                        class="footer-refresh-btn"
                                        onClick={() =>
                                            this.handleSubmit(
                                                submitActions.RESET
                                            )
                                        }
                                    >
                                        <i class="flaticon-refresh-arrow"></i>
                                    </div>
                                    <div
                                        class="footer-submit-btn"
                                        onClick={() =>
                                            this.handleSubmit(
                                                submitActions.SUBMIT
                                            )
                                        }
                                    >
                                        ثبت‌نهایی
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="search-row">
                            <div class="search-wrapper">
                                <input
                                    class="search-input"
                                    placeholder="نام درس به انگلیسی"
                                    value={this.search}
                                    onChange={(event) =>
                                        this.setState({
                                            search: event.target.value,
                                        })
                                    }
                                />
                                <div
                                    class="search-btn"
                                    onClick={(event) => {
                                        event.preventDefault();
                                        this.searchCourses();
                                    }}
                                >
                                    <div>جستجو</div>
                                    <i class="flaticon-loupe"></i>
                                </div>
                            </div>
                        </div>

                        <div class="table-wrapper-courses available-courses">
                            <div class="table-title-courses">
                                دروس ارائه شده
                            </div>

                            <div class="course-type-selector">
                                <div class={`course-type-item ${typeFilter == "" && "selected-item"}`} onClick={this.setTypeFilter("")}>همه</div>
                                <div class={`course-type-item ${typeFilter == "Takhasosi" && "selected-item"}`} onClick={this.setTypeFilter("Takhasosi")}>تخصصی</div>
                                <div class={`course-type-item ${typeFilter == "Asli" && "selected-item"}`} onClick={this.setTypeFilter("Asli")}>اصلی</div>
                                <div class={`course-type-item ${typeFilter == "Ekhtiary" && "selected-item"}`} onClick={this.setTypeFilter("Ekhtiary")}>اختیاری</div>
                                <div class={`course-type-item ${typeFilter == "Umumi" && "selected-item"}`} onClick={this.setTypeFilter("Umumi")}>عمومی</div>
                            </div>

                            <div class="available-courses-table-titles">
                                <div class="title action"></div>
                                <div class="title code">کد</div>
                                <div class="title capacity">ظرفیت</div>
                                <div class="title type">نوع</div>
                                <div class="title name">نام درس</div>
                                <div class="title teacher">استاد</div>
                                <div class="title units">واحد</div>
                                <div class="title description">توضیحات</div>
                            </div>
                            {this.state.courses.filter(c=>typeFilter == ""? true: c.type==typeFilter).map((course) => (
                                <div class="table-row-courses">
                                    <div
                                        class="table-cell action"
                                        onClick={() =>
                                            this.handleAddCourse(course)
                                        }
                                    >
                                        {course.signedUp < course.capacity ? (
                                            <i class="flaticon-add"></i>
                                        ) : (
                                            <i class="flaticon-clock-circular-outline"></i>
                                        )}
                                    </div>
                                    <div class="table-cell code">
                                        {convertNumbersToPersian(course.code)}
                                    </div>
                                    <div class="table-cell capacity">{`${convertNumbersToPersian(
                                        course.signedUp
                                    )}\/${convertNumbersToPersian(
                                        course.capacity
                                    )}`}</div>
                                    <div class="table-cell type">
                                        <div
                                            class={`type-value type-${
                                                courseTypes[course.type]
                                                    .className
                                            }`}
                                        >
                                            {courseTypes[course.type].fa}
                                        </div>
                                    </div>
                                    <div class="table-cell name">
                                        {course.name}
                                    </div>
                                    <div class="table-cell teacher">
                                        {course.Instructor
                                            ? course.Instructor
                                            : "نامشخص"}
                                    </div>
                                    <div class="table-cell units">
                                        {convertNumbersToPersian(course.units)}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        );
    }
}
