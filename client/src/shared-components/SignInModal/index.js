import React, { useState } from "react";
import { Modal, Button, InputGroup, FormControl, Alert } from "react-bootstrap";

export default function SignInModal(props) {
    const [stdId, setStdId] = useState("");
    const [error, setError] = useState(null);

    const handleSubmit = (event) => {
        event.preventDefault();

        props.handleSignIn(stdId, setError);

        setStdId("");
    };

    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>ورود</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Alert variant="danger" show={error}>
                    {error}
                </Alert>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="ایمیل"
                        aria-label="email"
                        aria-describedby="basic-addon1"
                        value={stdId}
                        onChange={(e) => setStdId(e.target.value)}
                    />
                </InputGroup>
                <InputGroup className="mb-2">
                    <FormControl
                        placeholder="رمز عبور"
                        aria-label="password"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleSubmit}>
                    ورود
                </Button>
                <Button variant="outline-secondary" onClick={props.handleClose}>
                    انصراف
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
